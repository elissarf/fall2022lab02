import java.util.Scanner;

public class HelloVsCode {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // Ask user for height

        System.out.println("What is your height in meters");
        double height = scan.nextDouble();

        // If height out of range then stop
        while (height > 2.2 || height < 0.2) {
            System.out.println("Your height is out of range, please enter a height between 0.2m-2.2m");
            height = scan.nextDouble();
        }
        // Ask user for weight
        System.out.println("What is your wheight in kgs");
        double weight = scan.nextDouble();

        // if weight out of range then stop
        while (weight > 300 || weight < 10) {
            System.out.println("Weight is out of range, please enter weight under 300 kg and over 10 kg");
            weight = scan.nextDouble();
        }
        System.out.println("your height is " + height + " m");
        System.out.println("your weight is " + weight + " kg");

        double bmi = weight / (height * height);

        System.out.println("your bmi is " + bmi);
        // Tell user Singaporean health risk
        // Tell user Underweight
        if (bmi < 18.5) {
            System.out.println("You are severely underweight");
        }
        // Tell user Normal Range
        if (bmi > 18.5 && bmi < 22.9) {
            System.out.println("You are in the Normal Range");
        }
        // Tell user Mild to moderate overweight
        if (bmi > 23.0 && bmi < 27.4) {
            System.out.println("You are in the Mild to moderate range");
        }
        // Tell user Very overweight to obese
        if (bmi >= 27.5) {
            System.out.println("You are in the very overweight category to obese");
        }

    }
}
